import 'package:demo/paquete_bdd.dart';
import 'package:partida/partida.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'dart:convert';

class AlmacenamientoLocal {
  Future<bool> eliminarUsuario() async {
    bool check = false;
    SharedPreferences prefs = await SharedPreferences.getInstance();
    check = await prefs.clear().then((bool success) {
      return success;
    });
    return check;
  }

  Future<List<Partida>> recuperarPartidas() async {
    List<Partida> partidas;
    SharedPreferences prefs = await SharedPreferences.getInstance();
    var respuesta = prefs.getString('usuario');
    var usuarioJson = jsonDecode(respuesta!);
    Usuarios usuario = Usuarios.fromJson(usuarioJson);
    return partidas = usuario.partidas;
  }

  Future<bool> registradoUsuario() async {
    late bool check;
    SharedPreferences prefs = await SharedPreferences.getInstance();
    var respuesta = prefs.getString('usuario');
    if (respuesta == null) {
      return check = false;
    }
    return check = true;
  }

  Future<bool> registrarPartida({required Partida partida}) async {
    bool check;
    SharedPreferences prefs = await SharedPreferences.getInstance();
    var respuesta = prefs.getString('usuario');
    var usuarioJson = jsonDecode(respuesta!);
    Usuarios usuario = Usuarios.fromJson(usuarioJson);
    usuario.partidas.add(partida);
    check = await registrarUsuario(usuario: usuario);
    return check;
  }

  Future<bool> registrarUsuario({required Usuarios usuario}) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    bool check;
    check = await prefs
        .setString('usuario', jsonEncode(usuario.toJson()))
        .then((bool success) {
      return success;
    });
    return check;
  }

  Future<Usuarios> recuperarUsuario() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    var respuesta = prefs.getString('usuario');
    var usuarioJson = jsonDecode(respuesta!);
    Usuarios usuario = Usuarios.fromJson(usuarioJson);
    return usuario;
  }

  Future<bool> reescribirUsuario({required Usuarios usuario}) async {
    bool check = false;
    SharedPreferences prefs = await SharedPreferences.getInstance();
    check = await prefs.clear().then((bool success) {
      return success;
    });
    if (check == true) {
      check = await prefs
          .setString('usuario', jsonEncode(usuario.toJson()))
          .then((bool success) {
        return success;
      });
    }
    return check;
  }

  Future<bool> actualizarDatosUsuario({required Usuarios usuarioNuevo}) async {
    bool check = false;
    SharedPreferences prefs = await SharedPreferences.getInstance();
    var respuesta = prefs.getString('usuario');
    var usuarioJson = jsonDecode(respuesta!);
    Usuarios usuarioViejo = Usuarios.fromJson(usuarioJson);
    usuarioViejo.nombre = usuarioNuevo.nombre;
    usuarioViejo.contra = usuarioNuevo.contra;
    check = await reescribirUsuario(usuario: usuarioViejo);
    return check;
  }

  Future<bool> eliminarPartida({required int indice}) async {
    bool check = false;
    SharedPreferences prefs = await SharedPreferences.getInstance();
    var respuesta = prefs.getString('usuario');
    var usuarioJson = jsonDecode(respuesta!);
    Usuarios usuarioViejo = Usuarios.fromJson(usuarioJson);
    usuarioViejo.partidas.removeAt(indice);
    check = await reescribirUsuario(usuario: usuarioViejo);
    return check;
  }
}

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:bloc/bloc.dart';
import 'package:ohanami/bloc/eventos.dart';
import 'package:partida/partida.dart';

import 'estados.dart';

class VistaBloc extends Bloc<Evento, Estados_ohanami> {
  VistaBloc(this.partida, this.iconosJugadores)
      : super(Ronda1(partida, iconosJugadores)) {
    on<SiguienteRonda1>(_onPartida1);
    on<SiguienteRonda2>(_onPartida2);
    on<SiguienteRonda3>(_onPartida3);
  }

  Partida partida;
  List<IconData> iconosJugadores;

  void _onPartida2(SiguienteRonda2 evento, Emitter<Estados_ohanami> emit) {
    partida = evento.partida;
    emit(Ronda2(partida, iconosJugadores));
  }

  void _onPartida3(SiguienteRonda3 evento, Emitter<Estados_ohanami> emit) {
    partida = evento.partida;

    emit(Ronda3(partida, iconosJugadores));
  }

  void _onPartida1(SiguienteRonda1 evento, Emitter<Estados_ohanami> emit) {
    partida = evento.partida;

    emit(Ronda1(partida, iconosJugadores));
  }
}

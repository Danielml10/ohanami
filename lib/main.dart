import 'package:flutter/material.dart';
import 'package:ohanami/vistas/vistaPrincipal.dart';

void main() => runApp(const MyApp());

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  static const String _title = 'Ohanami';

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData(
          brightness: Brightness.light, primaryColor: Colors.blueGrey),
      title: _title,
      debugShowCheckedModeBanner: false,
      home: VistaPrincipal(),
    );
  }
}

import 'package:demo/paquete_bdd.dart';
import 'package:flutter/material.dart';
import 'package:ohanami/app/constantes.dart';
import 'package:ohanami/bdd/almacenamientoLocal.dart';
import 'package:ohanami/vistas/vistaAgregarPartida.dart';
import 'package:ohanami/vistas/vistaEstadisticas.dart';
import 'package:ohanami/vistas/vistaInicio.dart';
import 'package:ohanami/vistas/vistaRegistro.dart';
import 'package:ohanami/vistas/vista_detalle.dart';
import 'package:ohanami/vistas/vista_login.dart';
import 'package:partida/partida.dart';

class VistaHome extends StatefulWidget {
  const VistaHome({Key? key}) : super(key: key);

  @override
  State<VistaHome> createState() => _MyStatefulWidgetState();
}

class _MyStatefulWidgetState extends State<VistaHome> {
  int _selectedIndex = 0;
  AlmacenamientoLocal local = AlmacenamientoLocal();
  late Future<Usuarios> usuario;

  static const List<Widget> _widgetOptions = <Widget>[
    VistaInicio(),
    VistaRegistro(),
  ];

  static const List<BottomNavigationBarItem> _navigationItems =
      <BottomNavigationBarItem>[
    BottomNavigationBarItem(icon: Icon(Icons.games), title: Text('Partidas')),
    BottomNavigationBarItem(
        icon: Icon(Icons.graphic_eq), title: Text('Estadísticas')),
  ];

  @override
  void initState() {
    usuario = local.recuperarUsuario();
    super.initState();
  }

  void sincronizarDB() async {
    AlmacenamientoLocal local = AlmacenamientoLocal();
    RepositorioMemoria mongo = RepositorioMemoria();
    Usuarios usuarioLocal = await local.recuperarUsuario();
    bool check = await mongo.inicializar();
    if (check == true) {
      check = await mongo.usuarioRegistrado(usuarios: usuarioLocal);
      if (check == true) {
        bool checo = await mongo.reescribirPartidas(usuarios: usuarioLocal);
      } else {
        bool check2 = await mongo.registrarUsuario(usuarios: usuarioLocal);
      }
    } else {}
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      floatingActionButton: FloatingActionButton(
          child: const Icon(
            Icons.add_circle,
          ),
          onPressed: () {
            Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (context) => const VistaAgregarPartida()));
          }),
      bottomNavigationBar: BottomNavigationBar(
        items: _navigationItems,
        selectedItemColor: Colors.red[800],
      ),
      appBar: AppBar(),
      body: Center(
          child: FutureBuilder<Usuarios>(
              future: local.recuperarUsuario(),
              builder:
                  (BuildContext context, AsyncSnapshot<Usuarios> snapshot) {
                return snapshot.hasData
                    ? snapshot.hasError
                        ? const Text('Error al obtener datos')
                        : snapshot.data!.partidas.isEmpty
                            ? Padding(
                                padding: const EdgeInsets.all(10.0),
                                child: Column(
                                  children: [
                                    SizedBox(
                                      height: 20,
                                    ),
                                    Center(
                                      child: Column(
                                        children: [
                                          Padding(
                                              padding: EdgeInsets.all(10.0),
                                              child:
                                                  Image.asset(partidasVacias)),
                                          Text('Aún no hay partidas:(')
                                        ],
                                      ),
                                    ),
                                  ],
                                ),
                              )
                            : _listaDatos(snapshot)
                    : const CircularProgressIndicator();
              })),
    );
  }

  _listaDatos(snapshot) {
    return ListView.builder(
        itemCount: snapshot.data!.partidas.length,
        itemBuilder: (BuildContext context, int index) {
          int partida = index + 1;
          int reverseIndex = snapshot.data!.partidas.length - 1 - index;
          FasePuntuacion.ronda1;
          FasePuntuacion.ronda2;
          FasePuntuacion.ronda3;
          snapshot.data!.partidas[0].puntuaciones(FasePuntuacion.ronda1);
          List<String> nombres = [];
          for (var i = 0;
              i < snapshot.data!.partidas[reverseIndex].jugadores.length;
              i++) {
            nombres.add(snapshot.data!.partidas[reverseIndex].jugadores
                .elementAt(i)
                .nombre
                .toString());
          }
          do {
            nombres.add("");
          } while (nombres.length < 4);

          return Card(
            elevation: 10.0,
            clipBehavior: Clip.antiAlias,
            child: Column(
              children: [
                ListTile(
                  leading: Icon(Icons.games),
                  title: Text('Partida ' + partida.toString()),
                  subtitle: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        'Jugadores',
                        style: TextStyle(color: Colors.black.withOpacity(0.6)),
                      ),
                      Text(nombres[0]),
                      Text(nombres[1]),
                      Text(nombres[2]),
                      Text(nombres[3]),
                    ],
                  ),
                ),
                ButtonBar(
                  alignment: MainAxisAlignment.start,
                  children: [
                    ElevatedButton(
                      onPressed: () {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => VistaDetalle(
                                    partida: snapshot
                                        .data!.partidas[reverseIndex])));
                      },
                      child: const Text('Ver Partida'),
                    ),
                    ElevatedButton(
                      onPressed: () async {
                        await local
                            .eliminarPartida(indice: reverseIndex)
                            .whenComplete(() {
                          setState(() {
                            snapshot.data!.partidas.removeAt(reverseIndex);
                          });
                        });
                      },
                      child: const Text('Eliminar partida'),
                    ),
                  ],
                ),
              ],
            ),
          );
        });
  }

  // ignore: non_constant_identifier_names
  _estadisticas_generales(snapshot) {
    return Card(
      child: Column(
        children: [Text('Partidas')],
      ),
    );
  }

  void alertDialog(BuildContext context) {
    var alert = AlertDialog(
      title: Text('¿Estas seguro de cerrar la sesion?'),
      content: Text("Se eliminarán todas las partidas que no estén en la nube"),
      actions: <Widget>[
        ElevatedButton(
            child: Text("Aceptar"),
            onPressed: () async {
              bool check = await local.eliminarUsuario();
              if (check == true) {
                print("Se elimino el usuario");
              }
              Navigator.of(context).pop();
              Navigator.push(context,
                  MaterialPageRoute(builder: (context) => VistaLogin()));
            }),
        ElevatedButton(
            child: Text("Cancelar"),
            onPressed: () {
              Navigator.of(context).pop();
            }),
      ],
    );
    showDialog(context: context, builder: (BuildContext context) => alert);
  }
}

import 'package:demo/paquete_bdd.dart';
import 'package:flutter/material.dart';
import 'package:ohanami/bdd/almacenamientoLocal.dart';
import 'package:ohanami/vistas/vistaHome.dart';

class VistaRegistro extends StatefulWidget {
  const VistaRegistro({Key? key}) : super(key: key);

  @override
  _VistaRegistroState createState() => _VistaRegistroState();
}

class _VistaRegistroState extends State<VistaRegistro> {
  TextEditingController nombre = TextEditingController();
  TextEditingController correo = TextEditingController();
  TextEditingController contra = TextEditingController();
  bool isLog = false;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Ohanami'),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            TextFormField(
              keyboardType: TextInputType.name,
              controller: nombre,
              decoration: const InputDecoration(
                labelStyle: TextStyle(),
                helperText: 'Nombre de usuario',
              ),
            ),
            TextFormField(
              keyboardType: TextInputType.emailAddress,
              controller: correo,
              decoration: const InputDecoration(
                labelStyle: TextStyle(),
                helperText: 'Correo',
              ),
            ),
            TextFormField(
              obscureText: true,
              keyboardType: TextInputType.visiblePassword,
              controller: contra,
              decoration: const InputDecoration(
                icon: Icon(Icons.password),
                labelStyle: TextStyle(),
                helperText: 'Contraseña',
              ),
            ),
            ElevatedButton(
              onPressed: () {
                print("empezo");
                validarLocal();
                ScaffoldMessenger.of(context).showSnackBar(
                    SnackBar(content: Text("Iniciando conexion")));
              },
              child: const Text(
                "Registrarse",
              ),
            )
          ],
        ),
      ),
    );
  }

  Future validacion() async {
    print('validar registro');
    RepositorioMemoria bdd = RepositorioMemoria();
    AlmacenamientoLocal local = AlmacenamientoLocal();
    local.eliminarUsuario();
    bool mongocheck = await bdd.inicializar();
    bool checkv = await local.registradoUsuario();

    Usuarios usuario = Usuarios(
        nombre: nombre.text,
        contra: contra.text,
        partidas: [],
        correo: correo.text);

    bool verificarUsuario = await bdd.usuarioRegistrado(usuarios: usuario);
    print('123424-323237627639736');
    if (mongocheck == true && checkv == true) {
      Usuarios usuario = await local.recuperarUsuario();
      checkv = await bdd.usuarioRegistrado(usuarios: usuario);
      if (checkv == true) {
        Usuarios u = await bdd.recuperarUsuario(usuario: usuario);
        checkv = await local.registrarUsuario(usuario: u);
        setState(() {
          isLog = checkv;
        });
      }
      setState(() {
        isLog = true;
      });
    }
    if (mongocheck == false && checkv == true) {
      setState(() {
        isLog = checkv;
      });
    }
  }

  void validarLocal() async {
    RepositorioMemoria bdd = RepositorioMemoria();
    AlmacenamientoLocal local = AlmacenamientoLocal();
    bool consultar = await local.registradoUsuario();
    if (consultar == true) {
      validarRegistro();
    }
    if (consultar == false) {
      Usuarios nuevoUsuario = Usuarios(
          nombre: nombre.text,
          correo: correo.text,
          contra: contra.text,
          partidas: []);
      bool usuariomongo = await bdd.usuarioRegistrado(usuarios: nuevoUsuario);

      if (usuariomongo) {
        bool consultar2 = await local.registrarUsuario(usuario: nuevoUsuario);

        if (consultar2 == true) {
          bool usuarioRegistradoConExito =
              await bdd.registrarUsuario(usuarios: nuevoUsuario);
        }
      }
    }
  }

  void validarRegistro() async {
    RepositorioMemoria bdd = RepositorioMemoria();
    Usuarios nuevoUsuario = Usuarios(
        nombre: nombre.text,
        correo: correo.text,
        contra: contra.text,
        partidas: []);
    bool nombreUsuarioEnUso =
        await bdd.usuarioRegistrado(usuarios: nuevoUsuario);
    if (nombreUsuarioEnUso == true) {
      print("Este nombre de usuario ya esta registrado");

      ScaffoldMessenger.of(context).showSnackBar(
          SnackBar(content: Text("Este nombre de usuario ya esta registrado")));
    }
    if (nombreUsuarioEnUso == false) {
      AlmacenamientoLocal local = AlmacenamientoLocal();
      bool usuarioLocalActualizado =
          await local.actualizarDatosUsuario(usuarioNuevo: nuevoUsuario);
      if (usuarioLocalActualizado == true) {
        Usuarios usuarioLocal = await local.recuperarUsuario();
        print("Usuario actualizado en local");
        RepositorioMemoria bdd = RepositorioMemoria();

        bool usuarioRegistradoConExito =
            await bdd.registrarUsuario(usuarios: usuarioLocal);
        if (usuarioRegistradoConExito == true) {
          print("usuario registrado en mongo");

          ScaffoldMessenger.of(context).showSnackBar(
              const SnackBar(content: Text("Usuario registrado en mongo")));
        }
        if (usuarioRegistradoConExito == false) {
          print("hubo un error al registrar");
          ScaffoldMessenger.of(context).showSnackBar(
              const SnackBar(content: Text("Error al registrar")));
        }
        Navigator.push(
            context, MaterialPageRoute(builder: (context) => VistaHome()));
      }
    }
  }
}

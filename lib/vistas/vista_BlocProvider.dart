import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:ohanami/bloc/bloc_principal.dart';
import 'package:ohanami/vistas/vista_dinamica.dart';
import 'package:partida/partida.dart';

class Vista_Bloc extends StatelessWidget {
  const Vista_Bloc(
      {Key? key, required this.partida, required this.iconosJugadores})
      : super(key: key);
  final Partida partida;
  final List<IconData> iconosJugadores;

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => VistaBloc(partida, iconosJugadores),
      child: MaterialApp(
        debugShowCheckedModeBanner: false,
        home: Scaffold(
          resizeToAvoidBottomInset: false,
          body: VistaDinamica(),
        ),
      ),
    );
  }
}

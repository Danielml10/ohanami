import 'package:demo/paquete_bdd.dart';
import 'package:flutter/material.dart';
import 'package:ohanami/app/constantes.dart';
import 'package:ohanami/app/estilos.dart';
import 'package:ohanami/app/widgets.dart';
import 'package:ohanami/bdd/almacenamientoLocal.dart';
import 'package:ohanami/vistas/vistaHome.dart';
import 'package:ohanami/vistas/vistaRegistro.dart';
import 'package:ohanami/vistas/vista_login.dart';

class VistaIniciandome extends StatefulWidget {
  const VistaIniciandome({Key? key}) : super(key: key);

  @override
  _VistaIniciandomeState createState() => _VistaIniciandomeState();
}

class _VistaIniciandomeState extends State<VistaIniciandome> {
  TextEditingController nombre = TextEditingController();
  TextEditingController correo = TextEditingController();
  TextEditingController pass = TextEditingController();
  RepositorioMemoria mongo = RepositorioMemoria();
  AlmacenamientoLocal local = AlmacenamientoLocal();

  @override
  Widget build(BuildContext context) {
    void sinconexion() async {
      Usuarios usuario =
          Usuarios(nombre: "", contra: '', partidas: [], correo: '');
      local.registrarUsuario(usuario: usuario);
      Navigator.push(
          context, MaterialPageRoute(builder: (context) => VistaHome()));
    }

    const EdgeInsets margen = EdgeInsets.only(left: 5.0, right: 10.0);
    String tituloAppBar = 'Ohanami';
    Widget registro = VistaRegistro();
    Widget login = VistaLogin();
    return Scaffold(
      appBar: PreferredSize(
        preferredSize: sizeAppBar,
        child: Text('Ohanami'),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            //Imagen Logo
            Container(
              margin: const EdgeInsets.fromLTRB(0.0, 5.0, 0.0, 0.0),
              padding: const EdgeInsets.all(2.0),
            ),
            //Bienvenido
            Container(
              margin: const EdgeInsets.fromLTRB(0.0, 10.0, 0.0, 10.0),
              child: Text(
                'Bienvenido',
                //style: style_titulo,
              ),
            ),
            /*BOTONES*/
            Column(
              mainAxisSize: MainAxisSize.max,
              children: [
                //Registro
                Container(
                  padding: const EdgeInsets.all(10.0),
                  margin: const EdgeInsets.only(right: 5.0),
                  //child: boton(registro, 'Registrarse'),
                ),
                //Iniciar Sesión
                Container(
                  padding: const EdgeInsets.all(10.0),
                  margin: const EdgeInsets.only(right: 5.0),
                  //child: boton(login, 'Iniciar Sesión'),
                ),
                //Boton Sin Conexión
                Container(
                  margin: margen,
                  padding: const EdgeInsets.all(8.0),
                  child: StreamBuilder(
                      builder: (BuildContext context, AsyncSnapshot snapshot) {
                    return Container(
                      margin: margen,
                      padding: const EdgeInsets.all(5.0),
                      child: StreamBuilder(builder:
                          (BuildContext context, AsyncSnapshot snapshot) {
                        return ElevatedButton(
                          onPressed: () {
                            sinconexion();
                            print('Sin conectar');
                          },
                          child: Text(
                            'Sin Conexión',
                            //style: btn_text,
                          ),
                          style: ButtonStyle(
                            padding: MaterialStateProperty.all<EdgeInsets>(
                                EdgeInsets.all(5.0)),

                            /*backgroundColor: MaterialStateProperty.resolveWith(
                                (states) => getColorFondo(states)),*/
                            minimumSize: MaterialStateProperty.all<Size>(
                                Size(200.0, 50.0)),
                          ),
                        );
                      }),
                    );
                  }),
                )
              ],
            ),
          ],
        ),
      ),
    );
  }
}

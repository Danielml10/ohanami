import 'package:flutter/material.dart';
import 'package:ohanami/vistas/vistaAgregarPartida.dart';

class VistaInicio extends StatelessWidget {
  const VistaInicio({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          Navigator.push(
            context,
            MaterialPageRoute(builder: (context) => VistaAgregarPartida()),
          );
        },
        backgroundColor: Colors.amber,
        child: const Icon(Icons.add),
      ),
    );
  }
}

import 'package:demo/paquete_bdd.dart';
import 'package:flutter/material.dart';
import 'package:ohanami/app/constantes.dart';
import 'package:ohanami/bdd/almacenamientoLocal.dart';
import 'package:ohanami/vistas/vistaHome.dart';
import 'package:ohanami/vistas/vistaRegistro.dart';

class VistaLogin extends StatefulWidget {
  const VistaLogin({Key? key}) : super(key: key);

  @override
  _VistaLoginState createState() => _VistaLoginState();
}

class _VistaLoginState extends State<VistaLogin> {
  //final _claveForm = GlobalKey<FormState>();

  TextEditingController correo = TextEditingController();
  TextEditingController contra = TextEditingController();
  RepositorioMemoria mongo = RepositorioMemoria();
  AlmacenamientoLocal local = AlmacenamientoLocal();

  void iniciarSesion(context) async {
    bool bandera = await mongo.inicializar();

    if (bandera == true) {
      Usuarios usuario = Usuarios(
          contra: contra.text.toString(),
          partidas: [],
          nombre: '',
          correo: correo.text.toString());
      bool sesionIniciada =
          await mongo.verificarInicioSesion(usuarios: usuario);
      if (sesionIniciada == true) {
        Usuarios usuarioMongo = await mongo.recuperarUsuario(usuario: usuario);
        local.registrarUsuario(usuario: usuarioMongo);
        print(usuarioMongo.toString());
        Navigator.push(context,
            MaterialPageRoute(builder: (context) => const VistaHome()));
      } else {
        print("Datos de inicio de sesion incorrectos");
      }
    } else {
      print("No hay conexion");
    }
  }

  void sinconexion() async {
    Usuarios usuario =
        Usuarios(nombre: "", contra: '', partidas: [], correo: '');
    local.registrarUsuario(usuario: usuario);
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Ohanami',
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        appBar: AppBar(),
        body: SingleChildScrollView(
          child: Column(
            children: [
              Container(
                padding: const EdgeInsets.all(10.0),
                child: Image.asset(imgOhanami),
                height: 200,
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Container(
                  child: TextField(
                    controller: correo,
                    keyboardType: TextInputType.emailAddress,
                    decoration: const InputDecoration(
                      icon: const Icon(Icons.account_circle),
                      labelText: 'Correo electrónico',
                    ),
                    onChanged: (value) {},
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Container(
                  child: TextField(
                    controller: contra,
                    obscureText: true,
                    keyboardType: TextInputType.visiblePassword,
                    decoration: const InputDecoration(
                      icon: const Icon(Icons.password_rounded),
                      labelText: 'Contraseña',
                    ),
                    onChanged: (value) {},
                  ),
                ),
              ),
              Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: ElevatedButton(
                    onPressed: () async {
                      ScaffoldMessenger.of(context).showSnackBar(
                          const SnackBar(content: Text("Iniciando conexion")));
                      iniciarSesion(context);
                    },
                    child: Container(
                      padding: const EdgeInsets.all(15.0),
                      child: const Text(
                        'Iniciar Sesion',
                        style: TextStyle(
                          fontSize: 16.0,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                    ),
                    style: ElevatedButton.styleFrom(
                        elevation: 10.0,
                        textStyle: const TextStyle(
                          fontSize: 20,
                        )),
                  )),
              Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: FlatButton(
                    onPressed: () async {
                      sinconexion();
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => const VistaRegistro()));
                    },
                    child: Container(
                      padding: const EdgeInsets.all(15.0),
                      child: const Text(
                        '¿Usuario nuevo? Regístrate',
                        style:
                            const TextStyle(fontSize: 15.0, color: Colors.blue),
                      ),
                    ),
                  )),
              Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: ElevatedButton(
                    onPressed: () async {
                      sinconexion();
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => const VistaHome()));
                    },
                    child: Container(
                      padding: const EdgeInsets.all(15.0),
                      child: const Text(
                        'Sin conexion',
                        style: TextStyle(
                          fontSize: 16.0,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                    ),
                    style: ElevatedButton.styleFrom(
                        elevation: 10.0,
                        textStyle: const TextStyle(
                          fontSize: 20,
                        )),
                  )),
            ],
          ),
        ),
      ),
    );
  }
}

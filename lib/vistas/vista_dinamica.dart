import 'package:flutter/material.dart';
import 'package:ohanami/bloc/bloc_principal.dart';
import 'package:ohanami/rondas/vista_ronda1.dart';
import 'package:ohanami/rondas/vista_ronda2.dart';
import 'package:ohanami/rondas/vista_ronda3.dart';
import 'package:provider/src/provider.dart';

class VistaDinamica extends StatelessWidget {
  const VistaDinamica({
    Key? key,
  }) : super(key: key);
  @override
  Widget build(BuildContext context) {
    final estado = context.watch<VistaBloc>().state;
    return estado.map(
      ronda1: (Ronda1) => VistaRonda1(
        partida: Ronda1.partida,
      ),
      ronda2: (Ronda2) => VistaRonda2(
        partida: Ronda2.partida,
      ),
      ronda3: (Ronda3) => VistaRonda3(
        partida: Ronda3.partida,
      ),
    );
  }
}

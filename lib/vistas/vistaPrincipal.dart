import 'dart:async';
import 'package:demo/paquete_bdd.dart';
import 'package:flutter/material.dart';
import 'package:ohanami/bdd/almacenamientoLocal.dart';
import 'package:ohanami/vistas/vistaHome.dart';
import 'vista_login.dart';

class VistaPrincipal extends StatefulWidget {
  const VistaPrincipal({Key? key}) : super(key: key);

  @override
  _VistaPrincipalState createState() => _VistaPrincipalState();
}

class _VistaPrincipalState extends State<VistaPrincipal> {
  bool bandera = false;
  //TextEditingController nombre = TextEditingController();
  //TextEditingController correo = TextEditingController();
  //TextEditingController clave = TextEditingController();

  @override
  void initState() {
    validacion().whenComplete(() async {
      Timer(const Duration(), () {
        bandera == false
            ? Navigator.push(context,
                MaterialPageRoute(builder: (context) => const VistaLogin()))
            : Navigator.push(context,
                MaterialPageRoute(builder: (context) => const VistaHome()));
      });
    });

    super.initState();
  }

  Future validacion() async {
    RepositorioMemoria mongo = RepositorioMemoria();
    AlmacenamientoLocal local = AlmacenamientoLocal();
    bool mongocheck = await mongo.inicializar();
    bool checkv = await local.registradoUsuario();

    if (mongocheck == true && checkv == true) {
      Usuarios usuarios = await local.recuperarUsuario();
      setState(() {
        bandera = true;
      });
    }
    if (mongocheck == false && checkv == true) {
      setState(() {
        bandera = checkv;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData(
          brightness: Brightness.dark, primaryColor: Colors.lightBlue),
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        body: Center(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: const [CircularProgressIndicator()],
          ),
        ),
      ),
    );
  }
}

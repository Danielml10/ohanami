import 'package:flutter/material.dart';
import 'package:ohanami/app/constantes.dart';
import 'estilos.dart';

/*class txtFPassword extends StatefulWidget {
  final TextEditingController pass;
  const txtFPassword({required this.pass, Key? key}) : super(key: key);

  @override
  _txtFPasswordState createState() => _txtFPasswordState();
}

class boton extends StatelessWidget {
  final Widget pagina;
  final String texto;
  const boton(
    this.pagina,
    this.texto, {
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ElevatedButton(
      onPressed: () {
        Navigator.push(
            context, MaterialPageRoute(builder: (context) => pagina));
      },
      child: Text(
        texto,
        style: btn_text,
      ),
      style: ButtonStyle(
        padding: MaterialStateProperty.all<EdgeInsets>(EdgeInsets.all(5.0)),
        textStyle: MaterialStateProperty.all<TextStyle>(btn_text),
        backgroundColor: MaterialStateProperty.resolveWith(
            (states) => getColorFondo(states)),
        minimumSize: MaterialStateProperty.all<Size>(Size(200.0, 50.0)),
      ),
    );
  }
}

class _txtFPasswordState extends State<txtFPassword> {
  bool _Esconder = true;
  @override
  Widget build(BuildContext context) {
    return TextFormField(
        controller: widget.pass,
        keyboardType: TextInputType.visiblePassword,
        validator: (value) =>
            value!.length < 6 ? 'La contraseña es muy corta' : null,
        autovalidateMode: AutovalidateMode.onUserInteraction,
        obscureText: _Esconder,
        decoration: InputDecoration(
            //Icono
            icon: password,
            //Datos
            hintText: 'Contraseña',
            hintStyle: txInfo,
            labelText: 'Contraseña',
            labelStyle: txNombre,
            //IconoDeVista
            suffixIcon: IconButton(
              onPressed: () {
                setState(() {
                  _Esconder = !_Esconder;
                });
              },
              icon: Icon(
                _Esconder ? visible : noVisible,
                color: const Color(cRosa1),
              ),
            ),
            //Estilo
            focusColor: Color(cRosa1),
            enabledBorder: OutlineInputBorder(
                borderSide: BorderSide(color: Color(cRosa1))),
            focusedBorder: OutlineInputBorder(
                borderSide: BorderSide(color: Color(cRosa3))),
            errorBorder:
                OutlineInputBorder(borderSide: BorderSide(color: Color(rojo))),
            focusedErrorBorder: OutlineInputBorder(
                borderSide: BorderSide(color: Color(rojo)))));
  }
}

Color getColorFondo(Set<MaterialState> states) {
  const Set<MaterialState> interactiveStates = <MaterialState>{
    MaterialState.pressed,
    MaterialState.hovered,
    MaterialState.focused,
  };
  if (states.any(interactiveStates.contains)) {
    return Color(cRosa3).withOpacity(.75);
  }
  return Color(cRosa3);
}

Widget appBar_Style(String _titulo) {
  return AppBar(
    title: Text(
      _titulo,
      style: styleText_bar,
    ),
    backgroundColor: Color(cRosa3),
  );
}

styleTFF(Icon icono, String txtAfuera, String txtDentro) {
  return InputDecoration(
      icon: icono,
      //Datos
      labelText: txtAfuera,
      labelStyle: txNombre,
      hintText: txtDentro,
      hintStyle: txInfo,
      //Estilo
      focusColor: Color(cRosa1),
      enabledBorder:
          OutlineInputBorder(borderSide: BorderSide(color: Color(cRosa1))),
      focusedBorder:
          OutlineInputBorder(borderSide: BorderSide(color: Color(cRosa3))),
      errorBorder:
          OutlineInputBorder(borderSide: BorderSide(color: Color(rojo))),
      focusedErrorBorder:
          OutlineInputBorder(borderSide: BorderSide(color: Color(rojo))));
}*/

import 'package:flutter/material.dart';

const String imgOhanami = 'assets/imagenes/ohanami.jpg';
const String partidasVacias = 'assets/imagenes/partidasVacias.jpg';

const sizeAppBar = Size.fromHeight(50.0);

const IconData visible = Icons.visibility;
const IconData noVisible = Icons.visibility_off;

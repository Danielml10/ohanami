import 'package:flutter/material.dart';
import 'package:ohanami/bloc/bloc_principal.dart';
import 'package:ohanami/bloc/eventos.dart';
import 'package:partida/partida.dart';
import 'package:provider/src/provider.dart';

class VistaRonda1 extends StatefulWidget {
  const VistaRonda1({Key? key, required this.partida}) : super(key: key);
  final Partida partida;
  @override
  _VistaRonda1State createState() => _VistaRonda1State(partida);
}

class _VistaRonda1State extends State<VistaRonda1> {
  _VistaRonda1State(this.partida);
  List<TextEditingController> _cartasAzules = [];
  Partida partida;

  void validarNumeroDeCartas(index) {
    try {
      List<CartasAPuntuarRonda1> lista = [];
      for (var i = 0; i < index; i++) {
        print("jugador " + (i + 1).toString());
        int azules = int.parse(_cartasAzules[i].text);
        CartasAPuntuarRonda1 cr1 = CartasAPuntuarRonda1(
          jugador: partida.jugadores.elementAt(i),
          cuantasAzules: azules,
        );
        lista.add(cr1);
      }
      partida.puntuacionRonda1(lista);
      partida.puntuacionesRonda1[0].cuantasAzules.toString();
      context.read<VistaBloc>().add(SiguienteRonda2(partida: partida));
    } on Exception catch (e) {}
  }

  _campoDeTexto(_var, _color, index, cartas) {
    _var.add(TextEditingController());
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Container(
        height: 80,
        width: 80,
        child: TextFormField(
          onChanged: (_) {},
          controller: _var[index],
          maxLength: 2,
          decoration: InputDecoration(
            counterText: "",
            focusColor: _color,
            focusedBorder:
                OutlineInputBorder(borderSide: BorderSide(color: _color)),
            labelText: cartas,
            labelStyle: TextStyle(color: _color, fontSize: 20),
            border: OutlineInputBorder(),
            enabledBorder: UnderlineInputBorder(
              borderSide: BorderSide(color: _color),
            ),
          ),
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          "Ronda 1",
          style: TextStyle(color: Colors.white),
        ),
      ),
      body: Center(
        child: Column(
          children: [
            Padding(
              padding: const EdgeInsets.all(8.0),
            ),
            SizedBox(
              height: 10.0,
            ),
            Expanded(
              child: ListView.builder(
                  padding: EdgeInsets.only(bottom: 80),
                  itemCount: partida.jugadores.length + 1,
                  itemBuilder: (BuildContext context, int index) {
                    return index == partida.jugadores.length
                        ? Container(
                            child: Center(
                                child: ElevatedButton(
                                    onPressed: () {
                                      validarNumeroDeCartas(index);
                                    },
                                    child: Text("Siguiente Ronda"))),
                          )
                        : Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Card(
                              clipBehavior: Clip.antiAlias,
                              child: Column(
                                children: [
                                  ListTile(
                                    title: Text(
                                      partida.jugadores
                                          .elementAt(index)
                                          .nombre
                                          .toString(),
                                      style: TextStyle(
                                          color: Colors.black87,
                                          fontSize: 20,
                                          fontStyle: FontStyle.normal),
                                    ),
                                  ),
                                  Row(
                                    children: [
                                      _campoDeTexto(_cartasAzules, Colors.blue,
                                          index, "Azules"),
                                    ],
                                  ),
                                ],
                              ),
                            ),
                          );
                  }),
            ),
          ],
        ),
      ),
    );
  }
}

import 'package:flutter/material.dart';
import 'package:ohanami/bloc/bloc_principal.dart';
import 'package:ohanami/bloc/eventos.dart';

import 'package:partida/partida.dart';
import 'package:provider/src/provider.dart';

class VistaRonda2 extends StatefulWidget {
  const VistaRonda2({Key? key, required this.partida}) : super(key: key);
  final Partida partida;

  @override
  State<VistaRonda2> createState() => _VistaRonda2State(partida);
}

_campoDeTexto(_var, _color, index, cartas) {
  _var.add(TextEditingController());
  return Padding(
    padding: const EdgeInsets.all(8.0),
    child: Container(
      height: 80,
      width: 80,
      child: TextFormField(
        onChanged: (_) {},
        controller: _var[index],
        maxLength: 2,
        decoration: InputDecoration(
          counterText: "",
          focusColor: _color,
          focusedBorder:
              OutlineInputBorder(borderSide: BorderSide(color: _color)),
          labelText: cartas,
          labelStyle: TextStyle(color: _color, fontSize: 20),
          border: OutlineInputBorder(),
          enabledBorder: UnderlineInputBorder(
            borderSide: BorderSide(color: _color),
          ),
        ),
      ),
    ),
  );
}

class _VistaRonda2State extends State<VistaRonda2> {
  List<TextEditingController> _cartasAzules = [];
  List<TextEditingController> _cartasVerdes = [];

  Partida partida;

  _VistaRonda2State(this.partida);
  void validarNumeroDeCartas(index) {
    try {
      List<CartasAPuntuarRonda2> lista = [];
      for (var i = 0; i < index; i++) {
        print("jugador " + (i + 1).toString());
        int azules = int.parse(_cartasAzules[i].text);
        int verdes = int.parse(_cartasVerdes[i].text);
        CartasAPuntuarRonda2 cr1 = CartasAPuntuarRonda2(
          jugador: partida.jugadores.elementAt(i),
          cuantasAzules: azules,
          cuantasVerdes: verdes,
        );
        lista.add(cr1);
      }
      partida.puntuacionRonda2(lista);
      context.read<VistaBloc>().add(SiguienteRonda3(partida: partida));
    } on Exception catch (e) {
      _MensajeScaffol(e.toString());
    }
  }

  void _MensajeScaffol(String mensaje) {
    ScaffoldMessenger.of(context)
        .showSnackBar(SnackBar(content: Text(mensaje)));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          "Ronda 2",
          style: TextStyle(color: Colors.white),
        ),
      ),
      body: Center(
        child: Column(
          children: [
            Padding(
              padding: const EdgeInsets.all(8.0),
            ),
            SizedBox(
              height: 10.0,
            ),
            Expanded(
              child: ListView.builder(
                  padding: EdgeInsets.only(bottom: 80),
                  itemCount: partida.jugadores.length + 1,
                  itemBuilder: (BuildContext context, int index) {
                    return index == partida.jugadores.length
                        ? Container(
                            child: Center(
                                child: ElevatedButton(
                                    onPressed: () {
                                      validarNumeroDeCartas(index);
                                    },
                                    child: Text("Siguiente Ronda"))),
                          )
                        : Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Card(
                              clipBehavior: Clip.antiAlias,
                              child: Column(
                                children: [
                                  ListTile(
                                    title: Text(
                                        partida.jugadores
                                            .elementAt(index)
                                            .nombre
                                            .toString(),
                                        style: TextStyle(
                                            color: Colors.black87,
                                            fontSize: 20,
                                            fontStyle: FontStyle.normal)),
                                  ),
                                  Row(
                                    children: [
                                      _campoDeTexto(_cartasAzules, Colors.blue,
                                          index, "Azules"),
                                      _campoDeTexto(_cartasVerdes, Colors.green,
                                          index, "Verdes"),
                                    ],
                                  ),
                                ],
                              ),
                            ),
                          );
                  }),
            ),
          ],
        ),
      ),
    );
  }
}

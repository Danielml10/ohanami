import 'dart:convert';
import 'package:collection/collection.dart';
import 'package:partida/partida.dart';

class Usuarios {
  String nombre;
  String correo;
  String contra;

  List<Partida> partidas;

  Usuarios(
      {required this.nombre,
      required this.correo,
      required this.contra,
      required this.partidas});

  Usuarios copyWith({
    String? nombre,
    String? correo,
    String? contra,
    List<Partida>? partidas,
  }) {
    return Usuarios(
      nombre: nombre ?? this.nombre,
      correo: correo ?? this.correo,
      contra: contra ?? this.contra,
      partidas: partidas ?? this.partidas,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'nombre': nombre,
      'correo': correo,
      'contra': contra,
      'partidas': partidas.map((x) => x.toMap()).toList(),
    };
  }

  factory Usuarios.fromMap(Map<String, dynamic> map) {
    return Usuarios(
      nombre: map['nombre'],
      correo: map['correo'],
      contra: map['contra'],
      partidas:
          List<Partida>.from(map['partidas']?.map((x) => Partida.fromMap(x))),
    );
  }

  String toJson() => json.encode(toMap());

  factory Usuarios.fromJson(String source) =>
      Usuarios.fromMap(json.decode(source));

  @override
  String toString() {
    return 'Usuarios(nombre: $nombre, correo: $correo, contra: $contra, partidas: $partidas)';
  }

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;
    final listEquals = const DeepCollectionEquality().equals;

    return other is Usuarios &&
        other.nombre == nombre &&
        other.correo == correo &&
        other.contra == contra &&
        listEquals(other.partidas, partidas);
  }

  @override
  int get hashCode {
    return nombre.hashCode ^
        correo.hashCode ^
        contra.hashCode ^
        partidas.hashCode;
  }
}

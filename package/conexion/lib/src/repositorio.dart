import 'package:partida/partida.dart';
import 'usuarios.dart';

abstract class Repositorio {
  /*bool registradoJugador(Jugador j);
  bool registrarJugador(Jugador j);
  void registrarPartida(Partida p, Jugador j);
  List<Partida> recuperarPartidas(Jugador j);*/
  Future<bool> registrarUsuario({required Usuarios usuarios});
  Future<bool> usuarioRegistrado({required Usuarios usuarios});
  Future<bool> eliminarJugador({required Usuarios usuarios});
  Future<bool> registrarPartida(
      {required Partida partida, required Usuarios usuarios});
  Future<List<Partida>> recuperarPartidas({required Usuarios usuarios});
  verificarInicioSesion({required Usuarios usuarios});
}

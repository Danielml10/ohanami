import 'dart:convert';
import 'dart:io';
import 'package:mongo_dart/mongo_dart.dart';
import 'package:partida/partida.dart';
import '../paquete_bdd.dart';
import 'constantes.dart';

class RepositorioMemoria extends Repositorio {
  late Db db;
  RepositorioMemoria();

  Future<bool> inicializar() async {
    bool consultar = false;
    try {
      await Db.create(url).then((value) => consultar = true);
    } on SocketException catch (_) {
      consultar = false;
    }
    return consultar;
  }

  @override
  Future<bool> registrarPartida(
      {required Partida partida, required Usuarios usuarios}) async {
    bool bandera = false;
    List<Partida> lista = await recuperarPartidas(usuarios: usuarios);
    var partidas = jsonDecode(partida.toJson());
    //var i = lista.length;
    db = await Db.create(url);
    await db.open();
    var coleccion = db.collection('usuarios');
    if (lista.isEmpty) {
      await coleccion
          .update(
              await coleccion
                  .findOne(where.eq('nombre', usuarios.nombre.toString())),
              SetStage({
                'partidas': [partidas],
              }).build())
          .then((value) => bandera = true);
    }
    await coleccion
        .update(
            await coleccion
                .findOne(where.eq('nombre', usuarios.nombre.toString())),
            Push({'partidas': partidas}).build())
        .then((value) => bandera = true);
    db.close();
    return bandera;
  }

  @override
  Future<List<Partida>> recuperarPartidas({required Usuarios usuarios}) async {
    List<Partida> lista;
    db = await Db.create(url);
    await db.open();
    var coleccion = db.collection('usuarios');
    var res =
        await coleccion.findOne(where.eq('nombre', usuarios.nombre.toString()));
    await db.close();
    res!.remove('_id');
    Usuarios user = Usuarios.fromMap(res);
    lista = user.partidas;
    return lista;
  }

  @override
  Future<bool> registrarUsuario({required Usuarios usuarios}) async {
    // TODO: implement registrarUsuario
    bool bandera = false;
    db = await Db.create(url);
    await db.open();
    var coleccion = db.collection('usuarios');
    await coleccion
        .insert(jsonDecode(usuarios.toJson()))
        .then((value) => bandera = true);
    db.close();
    return bandera;
  }

  @override
  Future<bool> usuarioRegistrado({required Usuarios usuarios}) async {
    print(usuarios.nombre);
    print(usuarios.contra);
    print(usuarios.partidas);
    db = await Db.create(url);
    await db.open();
    print('entró!');
    var coleccion = db.collection('usuarios');
    var res = await coleccion
        .find(where.eq('nombre', usuarios.nombre.toString()))
        .toList();
    db.close();
    if (res.isEmpty) {
      return false;
    }
    return true;
  }

  Future<bool> reescribirPartidas({required Usuarios usuarios}) async {
    bool check = false;
    List<Partida> partidas = usuarios.partidas;

    var partidas2 =
        jsonDecode(json.encode(partidas.map((x) => x.toMap()).toList()));

    print(partidas2);

    db = await Db.create(url);
    await db.open();
    var colexion = db.collection('usuarios');
    await colexion.updateOne(where.eq('nombre', usuarios.nombre.toString()),
        modify.set('partidas', partidas2));

    db.close();
    return check;
  }

  @override
  verificarInicioSesion({required Usuarios usuarios}) async {
    // TODO: implement verificarInicioSesion
    var db = await Db.create(url);
    await db.open();
    var coleccion = db.collection('usuarios');
    var res = await coleccion.findOne(where
        .eq('nombre', usuarios.nombre.toString())
        .and(where.eq('contra', usuarios.contra.toString())));
    db.close();
    if (res == null) {
      return false;
    }
    return true;
  }

  @override
  Future<bool> eliminarJugador({required Usuarios usuarios}) async {
    var bandera = false;
    var db = await Db.create(url);
    await db.open();
    var col = db.collection('usuarios');
    var elim = await col
        .deleteOne(where.eq('nombre', usuarios.nombre.toString()))
        .then((value) => bandera = true);
    db.close();
    return bandera;
  }

  Future<Usuarios> recuperarUsuario({required Usuarios usuario}) async {
    db = await Db.create(url);
    await db.open();
    var coleccion = db.collection('usuarios');
    var respuesta =
        await coleccion.findOne(where.eq('nombre', usuario.nombre.toString()));
    await db.close();
    respuesta!.remove('_id');
    Usuarios usuarioDB = Usuarios.fromMap(respuesta);
    return usuarioDB;
  }
}

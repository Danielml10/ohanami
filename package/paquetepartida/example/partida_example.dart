import 'package:partida/partida.dart';
import 'package:partida/src/puntuaciones.dart';

void main() {
  Jugador j1 = Jugador(nombre: 'Juan');
  Jugador j2 = Jugador(nombre: 'Pancho');
  Partida p = Partida(jugadores: {j1, j2})
    ..puntuacionRonda1(
        [CartasAPuntuarRonda1(jugador: j1, cuantasAzules: 3), CartasAPuntuarRonda1(jugador: j2, cuantasAzules: 1)])
    ..puntuacionRonda2([
      CartasAPuntuarRonda2(jugador: j1, cuantasAzules: 5, cuantasVerdes: 3),
      CartasAPuntuarRonda2(jugador: j2, cuantasAzules: 6, cuantasVerdes: 2)
    ]);
  print(p);
}

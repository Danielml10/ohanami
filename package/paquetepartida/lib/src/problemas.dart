class ProblemaNombreJugadorVacio extends Error implements Exception {
  @override
  String toString() => 'Ingrese el nombre de jugador';
}

class ProblemaNumeroJugadoresMenorMinimo extends Error implements Exception {
  @override
  String toString() => 'Ingrese más jugadores';
}

class ProblemaNumeroJugadoresMayorMaximo extends Error implements Exception {
  @override
  String toString() => 'Existen demasiados jugadores';
}

class ProblemaJugadoresRepetidos extends Error implements Exception {
  @override
  String toString() => 'Los jugadores no pueden repetirse';
}

class ProblemaAzulesNegativas extends Error implements Exception {
  @override
  String toString() =>
      'Cartas azules no pueden ser negativas. Agrega un valor válido';
}

class ProblemaJugadoresNoConcuerdan extends Error implements Exception {
  @override
  String toString() => 'Tus jugadores no son los mismos';
}

class ProblemaVerdesNegativas extends Error implements Exception {
  @override
  String toString() =>
      'Cartas verdes no pueden ser negativas. Agrega un valor válido';
}

class ProblemaOrdenIncorrecto extends Error implements Exception {
  @override
  String toString() => 'Los jugadores no concuerdan';
}

class ProblemaDisminucionAzules extends Error implements Exception {
  @override
  String toString() =>
      'El número de cartas azules ingresado es menor que el anterior.';
}

class ProblemaDisminucionVerdes extends Error implements Exception {
  @override
  String toString() =>
      'El número de cartas verdes ingresado es menor que el anterior.';
}

class ProblemasDemasiadasAzules extends Error implements Exception {
  @override
  String toString() => 'Hay demasiadas cartas azules.';
}

class ProblemaDemasiadasVerdes extends Error implements Exception {
  @override
  String toString() => 'Hay demasiadas cartas verdes.';
}

class ProblemaExcesoCartas extends Error implements Exception {
  @override
  String toString() => 'Hay demasiadas cartas en la partida.';
}

class ProblemaRosasNegativas extends Error implements Exception {
  @override
  String toString() =>
      'Cartas rosas no pueden ser negativas. Agrega un valor válido';
}

class ProblemaNegrasNegativas extends Error implements Exception {
  @override
  String toString() =>
      'Cartas negras no pueden ser negativas. Agrega un valor válido';
}

class ProblemaDemasiadasRosas extends Error implements Exception {
  @override
  String toString() => 'Hay demasiadas cartas rosas.';
}

class ProblemaDemasiadasNegras extends Error implements Exception {
  @override
  String toString() => 'Hay demasiadas cartas negras.';
}

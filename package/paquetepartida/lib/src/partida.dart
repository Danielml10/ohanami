import 'package:partida/partida.dart';
import 'package:partida/src/helpers.dart';
import 'dart:convert';
import 'package:collection/collection.dart';

const numeroMaximoJugadores = 4;
const numeroMinimoJugadores = 2;
const maximoCartasJugadasRonda2 = 20;
const maximoCartasJugadasRonda3 = 30;
const puntosPorAzul = 3;
const puntosPorVerde = 4;
const puntosPorNegra = 7;
var puntosPorRosa =
    {0, 1, 3, 6, 10, 15, 21, 28, 36, 45, 55, 66, 78, 91, 105, 120}.toList();

enum FasePuntuacion { ronda1, ronda2, ronda3, desenlace }

///Representa una partida de Ohanami
///

class Partida {
  ///Inicializa la partida con un set de [Jugador]
  ///Tira un [ProblemaNumeroJugadoresMenorMinimo] o [ProblemaNumeroJugadoresMayorMaximo]
  ///Si los jugadores no son diferentes tira un [ProblemaJugadoresRepetidos]

  Partida({required this.jugadores}) {
    if (jugadores.length < numeroMinimoJugadores)
      throw ProblemaNumeroJugadoresMenorMinimo();
    if (jugadores.length > numeroMaximoJugadores)
      throw ProblemaNumeroJugadoresMayorMaximo();
  }

  final Set<Jugador> jugadores;
  List<CartasAPuntuarRonda1> puntuacionesRonda1 = [];
  List<CartasAPuntuarRonda2> puntuacionesRonda2 = [];
  List<CartasAPuntuarRonda3> puntuacionesRonda3 = [];
  //final List<PuntuacionJugador> _puntuacionesJugador = [];

  Partida.constructor({
    required this.jugadores,
    required this.puntuacionesRonda1,
    required this.puntuacionesRonda2,
    required this.puntuacionesRonda3,
  });

  List<PuntuacionJugador> puntuaciones(FasePuntuacion ronda) {
    List<PuntuacionJugador> _puntuacionesJugador = [];
    switch (ronda) {
      case FasePuntuacion.ronda1:
        for (var pRonda1 in puntuacionesRonda1) {
          Jugador jugador = pRonda1.jugador;
          int azules = pRonda1.cuantasAzules;
          _puntuacionesJugador.add(PuntuacionJugador(
              jugador: jugador,
              porAzules: puntosPorAzul * azules,
              porVerdes: 0,
              porRosas: 0,
              porNegras: 0));
        }
        print('longitud en ronda 1');
        print(_puntuacionesJugador.length);

        return _puntuacionesJugador;

      case FasePuntuacion.ronda2:
        //print('cuantas verdes en el case Ronda2: ');
        //print(_puntuacionesRonda2[0].cuantasVerdes);
        for (var pRonda2 in puntuacionesRonda2) {
          Jugador jugador = pRonda2.jugador;
          int azules = pRonda2.cuantasAzules;
          int verdes = pRonda2.cuantasVerdes;
          _puntuacionesJugador.add(PuntuacionJugador(
              jugador: jugador,
              porAzules: azules * puntosPorAzul,
              porVerdes: verdes * puntosPorVerde,
              porRosas: 0,
              porNegras: 0));
        }
        print('longitud Ronda 2');
        print(_puntuacionesJugador.length);
        //print('total posicion Ronda 2');
        //print(_puntuacionesJugador[0].porAzules);
        //print(_puntuacionesJugador[0].porVerdes);
        //print(_puntuacionesJugador[0].total);
        return _puntuacionesJugador;

      case FasePuntuacion.ronda3:
        for (var pRonda3 in puntuacionesRonda3) {
          _puntuacionesJugador.add(PuntuacionJugador(
              jugador: pRonda3.jugador,
              porAzules: pRonda3.cuantasAzules * puntosPorAzul,
              porVerdes: pRonda3.cuantasVerdes * puntosPorVerde,
              porNegras: pRonda3.cuantasNegras * puntosPorNegra,
              porRosas: pRonda3.cuantasRosas > 15
                  ? 120
                  : puntosPorRosa[pRonda3.cuantasRosas]));
        }
        print('longitud ronda 3');
        print(_puntuacionesJugador.length);
        return _puntuacionesJugador;

      case FasePuntuacion.desenlace:
        // list<pjugador> r1 = puntuaciones(fase1);
        // list<pjugador> r1 = puntuaciones(fase1);
        // list<pjugador> r1 = puntuaciones(fase1);
        // r1+r2+r3

        for (Jugador j in jugadores) {
          /*int total = _puntuacionesJugador[i].total +
              _puntuacionesJugador[i + jugadores.length].total +
              _puntuacionesJugador[i + (jugadores.length * 2)].total;
          print(total);*/
          int azules = puntuaciones(FasePuntuacion.ronda1)
                  .firstWhere((element) => element.jugador == j)
                  .porAzules +
              puntuaciones(FasePuntuacion.ronda2)
                  .firstWhere((element) => element.jugador == j)
                  .porAzules +
              puntuaciones(FasePuntuacion.ronda3)
                  .firstWhere((element) => element.jugador == j)
                  .porAzules;
          int verdes = puntuaciones(FasePuntuacion.ronda1)
                  .firstWhere((element) => element.jugador == j)
                  .porVerdes +
              puntuaciones(FasePuntuacion.ronda2)
                  .firstWhere((element) => element.jugador == j)
                  .porVerdes +
              puntuaciones(FasePuntuacion.ronda3)
                  .firstWhere((element) => element.jugador == j)
                  .porVerdes;
          int negras = puntuaciones(FasePuntuacion.ronda1)
                  .firstWhere((element) => element.jugador == j)
                  .porNegras +
              puntuaciones(FasePuntuacion.ronda2)
                  .firstWhere((element) => element.jugador == j)
                  .porNegras +
              puntuaciones(FasePuntuacion.ronda3)
                  .firstWhere((element) => element.jugador == j)
                  .porNegras;
          int rosas = puntuaciones(FasePuntuacion.ronda1)
                  .firstWhere((element) => element.jugador == j)
                  .porRosas +
              puntuaciones(FasePuntuacion.ronda2)
                  .firstWhere((element) => element.jugador == j)
                  .porRosas +
              puntuaciones(FasePuntuacion.ronda3)
                  .firstWhere((element) => element.jugador == j)
                  .porRosas;
          _puntuacionesJugador.add(PuntuacionJugador(
              jugador: j,
              porAzules: azules,
              porVerdes: verdes,
              porRosas: rosas,
              porNegras: negras));
          //PuntuacionJugador p = PuntuacionJugador(jugador: j, porAzules: porAzules, porVerdes: porVerdes, porRosas: porRosas, porNegras: porNegras)
        }
        print('longitud desenlace');
        print(_puntuacionesJugador.length);
        //print('total posicion 6');
        //print(_puntuacionesJugador[6].porAzules);
        //print(_puntuacionesJugador[6].porVerdes);
        //print(_puntuacionesJugador[6].total);
        return _puntuacionesJugador;
    }
    //return [];
  }

  ///Guarda los datos de la primera ronda de puntuación de Ohanami
  ///
  ///En caso de que los jugadores de la puntuacions no coincidan con los
  ///del juego tira [ProblemaJugadoresNoConcuerdan]
  void puntuacionRonda1(List<CartasAPuntuarRonda1> puntuaciones) {
    Set<Jugador> jugadoresR1 = puntuaciones.map((e) => e.jugador).toSet();
    if (!setEquals(jugadores, jugadoresR1))
      throw ProblemaJugadoresNoConcuerdan();

    puntuacionesRonda1 = puntuaciones;
  }

  ///Guarda los datos de la segunda ronda de puntuación de Ohanami
  ///
  ///En caso de que los jugadores de las puntuaciones no coincida con los del juego
  ///se tira un [ProblemaJugadoresNoConcuerdan]
  ///Si se llama antes de puntuacionRonda1 se manda una excepción
  ///Si los números de las cartas no son los adecuados se pueden tirar otras excepciones
  void puntuacionRonda2(List<CartasAPuntuarRonda2> puntuaciones) {
    if (puntuacionesRonda1.isEmpty) throw ProblemaOrdenIncorrecto();

    Set<Jugador> jugadoresR2 = puntuaciones.map((e) => e.jugador).toSet();
    if (!setEquals(jugadores, jugadoresR2))
      throw ProblemaJugadoresNoConcuerdan();

    for (CartasAPuntuarRonda2 segundaPuntuacion in puntuaciones) {
      CartasAPuntuarRonda1 primeraPuntuacion = puntuacionesRonda1.firstWhere(
          (element) => element.jugador == segundaPuntuacion.jugador);
      if (primeraPuntuacion.cuantasAzules > segundaPuntuacion.cuantasAzules) {
        throw ProblemaDisminucionAzules();
      }
    }

    for (CartasAPuntuarRonda2 p in puntuaciones) {
      if (p.cuantasAzules > maximoCartasJugadasRonda2)
        throw ProblemasDemasiadasAzules();
      if (p.cuantasVerdes > maximoCartasJugadasRonda2)
        throw ProblemaDemasiadasVerdes();
      if ((p.cuantasVerdes + p.cuantasAzules) > maximoCartasJugadasRonda2) {
        throw ProblemaExcesoCartas();
      }
    }
    puntuacionesRonda2 = puntuaciones;
  }

  void puntuacionRonda3(List<CartasAPuntuarRonda3> puntuaciones) {
    if (puntuacionesRonda2.isEmpty) throw ProblemaOrdenIncorrecto();

    Set<Jugador> jugadoresR3 = puntuaciones.map((e) => e.jugador).toSet();
    if (!setEquals(jugadores, jugadoresR3))
      throw ProblemaJugadoresNoConcuerdan();

    for (CartasAPuntuarRonda3 p in puntuaciones) {
      if (p.cuantasAzules > maximoCartasJugadasRonda3)
        throw ProblemasDemasiadasAzules();
      if (p.cuantasVerdes > maximoCartasJugadasRonda3)
        throw ProblemaDemasiadasVerdes();
      if (p.cuantasNegras > maximoCartasJugadasRonda3)
        throw ProblemaRosasNegativas();
      if (p.cuantasRosas > maximoCartasJugadasRonda3)
        throw ProblemaNegrasNegativas();
      if ((p.cuantasVerdes +
              p.cuantasAzules +
              p.cuantasNegras +
              p.cuantasRosas) >
          maximoCartasJugadasRonda3) throw ProblemaExcesoCartas();
    }

    for (CartasAPuntuarRonda3 terceraPuntuacion in puntuaciones) {
      CartasAPuntuarRonda2 segundaPuntuacion = puntuacionesRonda2.firstWhere(
          (element) => element.jugador == terceraPuntuacion.jugador);
      if (segundaPuntuacion.cuantasAzules > terceraPuntuacion.cuantasAzules) {
        throw ProblemaDisminucionAzules();
      }
      if (segundaPuntuacion.cuantasVerdes > terceraPuntuacion.cuantasVerdes) {
        throw ProblemaDisminucionVerdes();
      }
    }

    puntuacionesRonda3 = puntuaciones;
  }

  Partida copyWith({
    Set<Jugador>? jugadores,
    List<CartasAPuntuarRonda1>? puntuacionesRonda1,
    List<CartasAPuntuarRonda2>? puntuacionesRonda2,
    List<CartasAPuntuarRonda3>? puntuacionesRonda3,
  }) {
    return Partida.constructor(
      jugadores: jugadores ?? this.jugadores,
      puntuacionesRonda1: puntuacionesRonda1 ?? this.puntuacionesRonda1,
      puntuacionesRonda2: puntuacionesRonda2 ?? this.puntuacionesRonda2,
      puntuacionesRonda3: puntuacionesRonda3 ?? this.puntuacionesRonda3,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'jugadores': jugadores.map((x) => x.toMap()).toList(),
      'puntuacionesRonda1': puntuacionesRonda1.map((x) => x.toMap()).toList(),
      'puntuacionesRonda2': puntuacionesRonda2.map((x) => x.toMap()).toList(),
      'puntuacionesRonda3': puntuacionesRonda3.map((x) => x.toMap()).toList(),
    };
  }

  factory Partida.fromMap(Map<String, dynamic> map) {
    return Partida.constructor(
      jugadores:
          Set<Jugador>.from(map['jugadores']?.map((x) => Jugador.fromMap(x))),
      puntuacionesRonda1: List<CartasAPuntuarRonda1>.from(
          map['puntuacionesRonda1']
              ?.map((x) => CartasAPuntuarRonda1.fromMap(x))),
      puntuacionesRonda2: List<CartasAPuntuarRonda2>.from(
          map['puntuacionesRonda2']
              ?.map((x) => CartasAPuntuarRonda2.fromMap(x))),
      puntuacionesRonda3: List<CartasAPuntuarRonda3>.from(
          map['puntuacionesRonda3']
              ?.map((x) => CartasAPuntuarRonda3.fromMap(x))),
    );
  }

  String toJson() => json.encode(toMap());

  factory Partida.fromJson(String source) =>
      Partida.fromMap(json.decode(source));

  @override
  String toString() {
    return 'Partida(jugadores: $jugadores, puntuacionesRonda1: $puntuacionesRonda1, puntuacionesRonda2: $puntuacionesRonda2, puntuacionesRonda3: $puntuacionesRonda3)';
  }

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;
    final collectionEquals = const DeepCollectionEquality().equals;

    return other is Partida &&
        collectionEquals(other.jugadores, jugadores) &&
        collectionEquals(other.puntuacionesRonda1, puntuacionesRonda1) &&
        collectionEquals(other.puntuacionesRonda2, puntuacionesRonda2) &&
        collectionEquals(other.puntuacionesRonda3, puntuacionesRonda3);
  }

  @override
  int get hashCode {
    return jugadores.hashCode ^
        puntuacionesRonda1.hashCode ^
        puntuacionesRonda2.hashCode ^
        puntuacionesRonda3.hashCode;
  }
}

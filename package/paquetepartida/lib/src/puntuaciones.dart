import '../partida.dart';
import 'dart:convert';

const int ninguna = 0;
const int maximoCartasPR1 = 10;
const int maximoCartasPR2 = 20;
const int maximoCartasPR3 = 30;

class CartasAPuntuarRonda1 {
  final Jugador jugador;
  final int cuantasAzules;

  CartasAPuntuarRonda1({required this.jugador, required this.cuantasAzules}) {
    if (cuantasAzules < ninguna) throw ProblemaAzulesNegativas();
    if (cuantasAzules > maximoCartasPR1) throw ProblemasDemasiadasAzules();
  }

  Map<String, dynamic> toMap() {
    return {
      'jugador': jugador.toMap(),
      'cuantasAzules': cuantasAzules,
    };
  }

  factory CartasAPuntuarRonda1.fromMap(Map<String, dynamic> map) {
    return CartasAPuntuarRonda1(
      jugador: Jugador.fromMap(map['jugador']),
      cuantasAzules: map['cuantasAzules'],
    );
  }

  String toJson() => json.encode(toMap());

  factory CartasAPuntuarRonda1.fromJson(String source) =>
      CartasAPuntuarRonda1.fromMap(json.decode(source));
}

class CartasAPuntuarRonda2 {
  final Jugador jugador;
  final int cuantasAzules;
  final int cuantasVerdes;

  CartasAPuntuarRonda2(
      {required this.jugador,
      required this.cuantasAzules,
      required this.cuantasVerdes}) {
    if (cuantasAzules < ninguna) throw ProblemaAzulesNegativas();
    if (cuantasVerdes < ninguna) throw ProblemaVerdesNegativas();
  }

  Map<String, dynamic> toMap() {
    return {
      'jugador': jugador.toMap(),
      'cuantasAzules': cuantasAzules,
      'cuantasVerdes': cuantasVerdes,
    };
  }

  factory CartasAPuntuarRonda2.fromMap(Map<String, dynamic> map) {
    return CartasAPuntuarRonda2(
      jugador: Jugador.fromMap(map['jugador']),
      cuantasAzules: map['cuantasAzules'],
      cuantasVerdes: map['cuantasVerdes'],
    );
  }

  String toJson() => json.encode(toMap());

  factory CartasAPuntuarRonda2.fromJson(String source) =>
      CartasAPuntuarRonda2.fromMap(json.decode(source));
}

class CartasAPuntuarRonda3 {
  final Jugador jugador;
  final int cuantasAzules;
  final int cuantasVerdes;
  final int cuantasNegras;
  final int cuantasRosas;
  CartasAPuntuarRonda3(
      {required this.jugador,
      required this.cuantasAzules,
      required this.cuantasVerdes,
      required this.cuantasNegras,
      required this.cuantasRosas}) {
    if (cuantasAzules < ninguna) throw ProblemaAzulesNegativas();
    if (cuantasVerdes < ninguna) throw ProblemaVerdesNegativas();
    if (cuantasNegras < ninguna) throw ProblemaNegrasNegativas();
    if (cuantasRosas < ninguna) throw ProblemaRosasNegativas();
  }

  Map<String, dynamic> toMap() {
    return {
      'jugador': jugador.toMap(),
      'cuantasAzules': cuantasAzules,
      'cuantasVerdes': cuantasVerdes,
      'cuantasNegras': cuantasNegras,
      'cuantasRosas': cuantasRosas,
    };
  }

  factory CartasAPuntuarRonda3.fromMap(Map<String, dynamic> map) {
    return CartasAPuntuarRonda3(
      jugador: Jugador.fromMap(map['jugador']),
      cuantasAzules: map['cuantasAzules'],
      cuantasVerdes: map['cuantasVerdes'],
      cuantasNegras: map['cuantasNegras'],
      cuantasRosas: map['cuantasRosas'],
    );
  }

  String toJson() => json.encode(toMap());

  factory CartasAPuntuarRonda3.fromJson(String source) =>
      CartasAPuntuarRonda3.fromMap(json.decode(source));
}
